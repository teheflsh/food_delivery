(function ($) {
  var cart_selector = '#views-form-commerce-cart-form-default',
  field = '#edit-quantity';
  function change_quantity(selector, a) {
    old_val = +$(selector).val();
    old_val = old_val + a;
    if (old_val < 0) {
      old_val = 0;
    }
    $(selector).val(String(old_val));
    return old_val;
  }

  function change_total_field(a) {
    price = $('#colorbox .views-field-commerce-price .field-content').html();
    price = price.substring(1);
    price = +price;
    total = price * a;
    total = total.toFixed(2);
    total = "$" + total;
    $('#colorbox .price').html(total);
  }

  function change_quantity_default_cart(a, selector_field, object) {
    index = $(object).index(selector_field);
    selector = cart_selector + '#edit-edit-quantity-' + index;
    change_quantity(selector,a);
    $(cart_selector + '#edit-submit').trigger('mousedown');
  }

  $('#colorbox').ready(function () {
    $(document).on('click', '#colorbox .plus', function () {
      change_total_field(change_quantity(field,1));
    });

    $(document).on('click', '#colorbox .minus', function () {
      change_total_field(change_quantity(field,-1));
    });
    $(document).on('change', field, function () {
      val = +$(field).val();
      if (val < 0) {
        val = 0;
        $(field).val('0');
      }
      change_total_field(val);
    })
    $(document).on('click', cart_selector + ' .plus', function () {
      change_quantity_default_cart(1, cart_selector + ' .plus', this)
    })
    $(document).on('click', cart_selector + ' .minus', function () {
      change_quantity_default_cart(-1, cart_selector + ' .minus', this)
    })
    $(document).on('change', cart_selector + ' .form-text', function () {
      $(cart_selector + ' #edit-submit').trigger('mousedown');
    })
    $(document).on('click', '#delete', function () {
      index = $(this).index(cart_selector + ' #delete');
      selector = cart_selector + ' #edit-edit-quantity-' + index;
      $(selector).val('0');
      $(cart_selector + ' #edit-submit').trigger('mousedown');
    })
  });
})(jQuery);