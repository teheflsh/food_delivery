/**
 * @file
 * Accordion
 */

(function ($) {
  // After click on show/hide button.
  $(document).on('click','.show-hide-button', function () {
    $(this).parent().toggleClass('expanded collapsed');
  })
})(jQuery);
