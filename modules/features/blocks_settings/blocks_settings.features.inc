<?php
/**
 * @file
 * blocks_settings.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function blocks_settings_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "boxes" && $api == "box") {
    return array("version" => "1");
  }
}
