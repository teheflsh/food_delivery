<?php
/**
 * @file
 * contetnt_products.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function contetnt_products_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Celery',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 3,
    'uuid' => '01ae38f4-43e4-4fc7-9708-29e0d36ef7d4',
    'vocabulary_machine_name' => 'products_categories',
    'url_alias' => array(
      0 => array(
        'alias' => 'catalog/celery',
        'language' => 'und',
      ),
    ),
    'parent' => array(
      0 => 'ade1f377-fe46-4f5b-bdf9-ea5829d39c03',
    ),
  );
  $terms[] = array(
    'name' => 'carrots',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '0a7291ad-fb96-4411-b6f8-40492e795f7b',
    'vocabulary_machine_name' => 'tags',
    'url_alias' => array(),
  );
  $terms[] = array(
    'name' => 'Radishes',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 6,
    'uuid' => '23a36313-fc37-4ba6-8a77-7eb7d30ed818',
    'vocabulary_machine_name' => 'products_categories',
    'url_alias' => array(
      0 => array(
        'alias' => 'catalog/radishes',
        'language' => 'und',
      ),
    ),
    'parent' => array(
      0 => 'ade1f377-fe46-4f5b-bdf9-ea5829d39c03',
    ),
  );
  $terms[] = array(
    'name' => 'parsley',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '2924bd5b-6180-469f-9cb8-adf297e2aee2',
    'vocabulary_machine_name' => 'tags',
    'url_alias' => array(),
  );
  $terms[] = array(
    'name' => 'Apricot',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 1,
    'uuid' => '2ceddc4a-a3d0-420a-b5c4-b315f3b72cc1',
    'vocabulary_machine_name' => 'products_categories',
    'url_alias' => array(),
    'parent' => array(
      0 => 'e57472bd-a685-46bb-a44f-a71401fbb382',
    ),
  );
  $terms[] = array(
    'name' => 'potatoes',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '3a97e680-7366-4fcb-8b07-fae1de041022',
    'vocabulary_machine_name' => 'tags',
    'url_alias' => array(),
  );
  $terms[] = array(
    'name' => 'vegetables',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '6da07a93-649e-444e-b5d8-029bc621c41e',
    'vocabulary_machine_name' => 'tags',
    'url_alias' => array(),
  );
  $terms[] = array(
    'name' => 'Pear',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 4,
    'uuid' => '71d712ea-911b-4ba9-a6f8-3fc21bb30238',
    'vocabulary_machine_name' => 'products_categories',
    'url_alias' => array(),
    'parent' => array(
      0 => 'e57472bd-a685-46bb-a44f-a71401fbb382',
    ),
  );
  $terms[] = array(
    'name' => 'test',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '73cfa0d3-94b5-496a-8672-918515653840',
    'vocabulary_machine_name' => 'tags',
    'url_alias' => array(),
  );
  $terms[] = array(
    'name' => 'roots',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '7b4b0545-54fb-40fa-acfe-b1062740c0dc',
    'vocabulary_machine_name' => 'tags',
    'url_alias' => array(),
  );
  $terms[] = array(
    'name' => 'Potatoes',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '7be7e83c-fe79-48e6-a2fb-8e535f41aae0',
    'vocabulary_machine_name' => 'products_categories',
    'url_alias' => array(
      0 => array(
        'alias' => 'catalog/potatoes',
        'language' => 'und',
      ),
    ),
    'parent' => array(
      0 => 'ade1f377-fe46-4f5b-bdf9-ea5829d39c03',
    ),
  );
  $terms[] = array(
    'name' => 'Beets',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 1,
    'uuid' => '83e7217a-f6ba-46a6-89f4-aa06b869b2c1',
    'vocabulary_machine_name' => 'products_categories',
    'url_alias' => array(
      0 => array(
        'alias' => 'catalog/beets',
        'language' => 'und',
      ),
    ),
    'parent' => array(
      0 => 'ade1f377-fe46-4f5b-bdf9-ea5829d39c03',
    ),
  );
  $terms[] = array(
    'name' => 'Peach',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 3,
    'uuid' => '866bea1e-0d0f-4132-8846-c07900bb7906',
    'vocabulary_machine_name' => 'products_categories',
    'url_alias' => array(),
    'parent' => array(
      0 => 'e57472bd-a685-46bb-a44f-a71401fbb382',
    ),
  );
  $terms[] = array(
    'name' => 'Parsley',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 4,
    'uuid' => '8bdf4dc3-5e40-41f6-9f30-1ed4e70a5625',
    'vocabulary_machine_name' => 'products_categories',
    'url_alias' => array(
      0 => array(
        'alias' => 'catalog/parsley',
        'language' => 'und',
      ),
    ),
    'parent' => array(
      0 => 'ade1f377-fe46-4f5b-bdf9-ea5829d39c03',
    ),
  );
  $terms[] = array(
    'name' => 'beets',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '8f8851dc-d69b-44d5-b18f-c711ce2b3f27',
    'vocabulary_machine_name' => 'tags',
    'url_alias' => array(),
  );
  $terms[] = array(
    'name' => 'radishes',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '8fdb589b-ae20-4560-91a9-da9efeb9148f',
    'vocabulary_machine_name' => 'tags',
    'url_alias' => array(),
  );
  $terms[] = array(
    'name' => 'Carrots',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 2,
    'uuid' => '96f1e3a3-8fc2-463f-87c7-143c55dafbf9',
    'vocabulary_machine_name' => 'products_categories',
    'url_alias' => array(
      0 => array(
        'alias' => 'catalog/carrots',
        'language' => 'und',
      ),
    ),
    'parent' => array(
      0 => 'ade1f377-fe46-4f5b-bdf9-ea5829d39c03',
    ),
  );
  $terms[] = array(
    'name' => 'Cherry',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 2,
    'uuid' => '9b1804f2-1599-4c29-9bd0-537b087c7316',
    'vocabulary_machine_name' => 'products_categories',
    'url_alias' => array(),
    'parent' => array(
      0 => 'e57472bd-a685-46bb-a44f-a71401fbb382',
    ),
  );
  $terms[] = array(
    'name' => 'tubers',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '9b6e7b1a-995d-4968-a93f-435676cb1b5d',
    'vocabulary_machine_name' => 'tags',
    'url_alias' => array(),
  );
  $terms[] = array(
    'name' => 'radish',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'ac4618a5-1661-434c-a1f7-9e95339357f5',
    'vocabulary_machine_name' => 'tags',
    'url_alias' => array(),
  );
  $terms[] = array(
    'name' => 'Vegetables',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 7,
    'uuid' => 'ade1f377-fe46-4f5b-bdf9-ea5829d39c03',
    'vocabulary_machine_name' => 'products_categories',
    'url_alias' => array(
      0 => array(
        'alias' => 'catalog/vegetables',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Apples',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'c2f65433-ef6e-4582-86c5-e3b333bf3ba4',
    'vocabulary_machine_name' => 'products_categories',
    'url_alias' => array(),
    'parent' => array(
      0 => 'e57472bd-a685-46bb-a44f-a71401fbb382',
    ),
  );
  $terms[] = array(
    'name' => 'vegetables',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'c57cc0fd-bb78-4cd2-b268-9a6b87b24e66',
    'vocabulary_machine_name' => 'tags',
    'url_alias' => array(),
  );
  $terms[] = array(
    'name' => 'celery',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'c6842dde-be2b-472e-a64c-3db3b45c15a2',
    'vocabulary_machine_name' => 'tags',
    'url_alias' => array(),
  );
  $terms[] = array(
    'name' => 'Radish',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 5,
    'uuid' => 'd040e8d8-afe0-4ea2-bbea-9dae934c2e7a',
    'vocabulary_machine_name' => 'products_categories',
    'url_alias' => array(
      0 => array(
        'alias' => 'catalog/radish',
        'language' => 'und',
      ),
    ),
    'parent' => array(
      0 => 'ade1f377-fe46-4f5b-bdf9-ea5829d39c03',
    ),
  );
  $terms[] = array(
    'name' => 'rosaceae',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'd5273128-4ab0-43b7-b685-4ee7b0802d95',
    'vocabulary_machine_name' => 'tags',
    'url_alias' => array(),
  );
  $terms[] = array(
    'name' => 'tubers',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'd78028c6-346e-417b-88d4-a48d79ece10c',
    'vocabulary_machine_name' => 'tags',
    'url_alias' => array(),
  );
  $terms[] = array(
    'name' => 'Plum',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 5,
    'uuid' => 'dbbe6720-f20b-4bc3-a209-4950f54742f8',
    'vocabulary_machine_name' => 'products_categories',
    'url_alias' => array(),
    'parent' => array(
      0 => 'e57472bd-a685-46bb-a44f-a71401fbb382',
    ),
  );
  $terms[] = array(
    'name' => 'Fruits',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'e57472bd-a685-46bb-a44f-a71401fbb382',
    'vocabulary_machine_name' => 'products_categories',
    'url_alias' => array(
      0 => array(
        'alias' => 'catalog/fruits',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'fruits',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => 'f5ecb005-dda7-4c02-9381-9741360c1130',
    'vocabulary_machine_name' => 'tags',
    'url_alias' => array(),
  );
  return $terms;
}
