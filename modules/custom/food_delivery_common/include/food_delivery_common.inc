<?php

/**
 * @file
 * The functional form.
 */

/**
 * Draw form.
 */
function food_delivery_common_add_cart($form, &$form_state) {
  commerce_cart_add_to_cart_form_submit($form, $form_state);
  $path = "<front>";
  ctools_include('ajax');
  ctools_add_js('ajax-responder');
  $commands[] = ctools_ajax_command_redirect($path);
  print ajax_render($commands);
  exit;
}
